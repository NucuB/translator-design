package inputOutput;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class FileReader extends JPanel implements ActionListener {
JButton go;
public String filePath;
JFileChooser chooser;
String choosertitle;

public FileReader() {
 go = new JButton("Browse");
 go.addActionListener(this);
 add(go);
 JFrame frame = new JFrame("");
 frame.addWindowListener(
   new WindowAdapter() {
     public void windowClosing(WindowEvent e) {
       System.exit(0);
       }
     }
   );
 frame.getContentPane().add(this,"Center");
 frame.setSize(400,200);
 frame.setVisible(true);
}

@Override
public void actionPerformed(ActionEvent arg0) {
	chooser = new JFileChooser(); 
	 chooser.setCurrentDirectory(new java.io.File("."));
	 chooser.setDialogTitle(choosertitle);
	 chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
	 chooser.setAcceptAllFileFilterUsed(true);
	 if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) { 
	   System.out.println("getCurrentDirectory(): " 
	      +  chooser.getCurrentDirectory());
	   System.out.println("getSelectedFile() : " 
	      +  chooser.getSelectedFile());
	   filePath = chooser.getSelectedFile().getPath();
	   }
	 else {
	   System.out.println("No Selection ");
	   }
	  }
}
