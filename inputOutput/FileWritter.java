package inputOutput;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class FileWritter {

	private RandomAccessFile file;

	public FileWritter(String file) {
		try {
			this.file = new RandomAccessFile(file, "rw");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void writeIntegerInFile(int number) {
		try {
			file.writeInt(number);
			System.out.println("Number written is : " + number);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void closeFileWriter() {
		try {
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public long getFileSize() {
		try {
			return file.length();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return 0;
	}

}
