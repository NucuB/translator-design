# Bisonita Compiler

## Scope

	This repository contains laboratory homeworks. According to laboratory requirements, I have developed a compiler using Java.

## Topics

* JavaCC
* Parse FLEX files
* ConditionToken parser
* Abstract Syntax Tree(JJTree)
* Ant build

