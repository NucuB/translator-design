package scanning;
public interface sym {

  /* terminals */

  public static final int TIMES = 10;

  public static final int LT = 12;

  public static final int OVER = 11;

  public static final int ELSE = 2;

  public static final int ENDIF= 17;

  public static final int PLUS = 8;

  public static final int RBRACE = 26;

  public static final int RPAREN = 22;

  public static final int INTEGER = 4;

  public static final int SEMI = 19;

  public static final int WHILE = 7;

  public static final int RETURN = 5;

  public static final int IF = 3;

  public static final int LBRACE = 25;

  public static final int LBRACKET = 23;

  public static final int LPAREN = 21;

  public static final int GT = 14;

  public static final int ID = 27;

  public static final int RBRACKET = 24;

  public static final int NUM = 28;

  public static final int COMMA = 20;

  public static final int LTEQ = 13;

  public static final int EOF = 0;

  public static final int MINUS = 9;

  public static final int error = 1;

  public static final int ASSIGN = 18;

  public static final int EQ = 16;

  public static final int GTEQ = 15;

  public static final int FOR = 6;
  
  public static final int ENDFOR = 30;
  
  public static final int ENDDO = 31;

  public static final int REAL = 32;
  
  public static final int PROGRAM = 33;
  
  public static final int ENDPROGRAM = 34;
  
  public static final int STRING = 35;
  
  public static final String[] terminalNames = new String[] {

  "EOF",

  "error",

  "ELSE",

  "IF",

  "INTEGER",
  
  "STRING",
  
  "REAL",
  
  "ENDFOR",
  
  "FOR",
  
  "ENDDO",
  
  "ENDIF",
  
  "RETURN",

  "WHILE",

  "PLUS",

  "MINUS",

  "TIMES",

  "OVER",

  "LT",

  "LTEQ",

  "GT",

  "GTEQ",

  "EQ",

  "ASSIGN",

  "SEMI",

  "COMMA",

  "LPAREN",

  "RPAREN",

  "LBRACKET",

  "RBRACKET",

  "LBRACE",

  "RBRACE",

  "ID",

  "NUM",
  
  "PROGRAM",
  "ENDPROGRAM"

  };

}

