package scanning;


import java.io.IOException;


import java_cup.runtime.Symbol;

public class Application {

	public static void main(String[] args) throws IOException {
	
		String input = "inputBisonita.txt";
		Lexer lexer = new Lexer(new java.io.FileReader(input));

		Symbol token = lexer.next_token();

		while (token.sym != sym.EOF) {
			System.out.println(token);
			token = lexer.next_token();
		}

	}

}
